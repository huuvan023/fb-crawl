const pptr = require('puppeteer')
require('dotenv').config();

let instance = null;

module.exports.getBrowserInstance = async function() {
    if (!instance)
        instance = await pptr.launch({
            executablePath: process.env.EXECUTABLE_PATH,
            headless: true,
            args: ['--disable-gpu']
        });
    return instance;
}