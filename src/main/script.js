module.exports = function() {
    return new Promise((resolve, reject) => {
        const PAGE_TO_SCROLL = 3;
        // Class for Individual Thread 
        // const C_THREAD = '.pagedlist_item:not(.pagedlist_hidden)';
        const C_THREAD = 'article._55wo._5rgr._5gh8.async_like'; //_55wo _5rgr _5gh8 async_like  
        // Class for threads marked for deletion on subsequent loop
        const C_THREAD_TO_REMOVE = 'article._55wo._5rgr._5gh8.async_like .TO_REMOVE';
        // Class for post time
        //const C_THREAD_TITLE = 'div._5rgt._5nk5._5msi p';
        const C_THREAD_POSTTIME = 'header div._4g34 div._78cz > a > abbr';
        // Class for postlink
        const C_THREAD_POSTLINK = 'header div._4g34 div._78cz > a'
            // Class for Description
            // const C_THREAD_DESCRIPTION = '.search_result_snippet .search_result_snippet .rendered_qtext ';
        const C_THREAD_DESCRIPTION = 'div._5rgt._5nk5._5msi p';
        // Class for line count
        const C_THREAD_LIKECOUNT = '._1w1k > ._1g06';
        // Class for comment
        const C_THREAD_CMT = '._1fnt > span._1j-c'
            // DOM attribute for link
        const A_THREAD_URL = 'href';
        // DOM attribute for ID
        const A_THREAD_ID = 'id';

        const _log = console.info,
            _warn = console.warn,
            _error = console.error,
            _time = console.time,
            _timeEnd = console.timeEnd;

        _time("Scrape");

        let page = 1;

        // Global Set to store all entries
        let threads = new Set(); // Eliminates dupes

        // Pause between pagination
        const PAUSE = 7000;

        // Accepts a parent DOM element and extracts the title and URL
        function scrapeSingleThread(elThread) {
            try {
                const elPostTime = elThread.querySelector(C_THREAD_POSTTIME) ? elThread.querySelector(C_THREAD_POSTTIME) : null,
                    elLink = elThread.querySelector(C_THREAD_POSTLINK) ? elThread.querySelector(C_THREAD_POSTLINK) : null,
                    elLikeCount = elThread.querySelector(C_THREAD_LIKECOUNT) ? elThread.querySelector(C_THREAD_LIKECOUNT) : null,
                    elCommentCount = elThread.querySelector(C_THREAD_CMT) ? elThread.querySelector(C_THREAD_CMT) : null,
                    elDescription = elThread.querySelector(C_THREAD_DESCRIPTION) ? elThread.querySelector(C_THREAD_DESCRIPTION) : null;
                if (elPostTime) {
                    const time = elPostTime ? elPostTime.innerText.trim() : 'null',
                        description = elDescription ? elDescription.innerText.trim() : null,
                        url = elLink ? elLink.getAttribute(A_THREAD_URL) : null;
                    likeCount = elLikeCount ? elLikeCount.innerText.trim() : 0;
                    commentCount = elCommentCount ? elCommentCount.innerText.trim() : 0;

                    if (!time.includes('at')) {
                        let tail = time.trim().split(' ')[1];
                        let pre = time.trim().split(' ')[0];

                        tail = tail.toLowerCase();
                        tail = tail.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
                        tail = tail.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
                        tail = tail.replace(/ì|í|ị|ỉ|ĩ/g, "i");
                        tail = tail.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
                        tail = tail.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
                        tail = tail.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
                        tail = tail.replace(/đ/g, "d");
                        tail = tail.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, "");
                        tail = tail.replace(/\u02C6|\u0306|\u031B/g, "");
                        tail = tail.trim();

                        if (tail.startsWith('min') || tail.startsWith('phut')) {
                            if (pre <= 30)
                                threads.add({
                                    time,
                                    description,
                                    url,
                                    reactCount: likeCount,
                                    commentCount
                                });
                        }
                    }
                    //     if (tail.startsWith('hr')) {
                    //         if (pre < 4) {
                    //             threads.add({
                    //                 time,
                    //                 description,
                    //                 url,
                    //                 reactCount: likeCount,
                    //                 commentCount
                    //             });
                    //         }
                    //     }
                    // }
                }

            } catch (e) {
                _error("Error capturing individual thread", e);
            }
        }

        // Get all threads in the visible context
        function scrapeThreads() {
            _log(`Scraping page ${page}`);
            const visibleThreads = document.querySelectorAll(C_THREAD);

            if (visibleThreads.length > 0) {
                _log(`Scraping page ${page}... found ${visibleThreads.length} threads`);
                Array.from(visibleThreads).forEach(scrapeSingleThread);
            } else {
                _warn(`Scraping page ${page}... found no threads`);
            }

            // Return master list of threads;
            // resolve(Array.from(threads));
            return visibleThreads.length;
        }

        // Clears the list between pagination to preserve memory
        // Otherwise, browser starts to lag after about 1000 threads
        function clearList() {
            _log(`Clearing list page ${page}`);
            const toRemove = `${C_THREAD_TO_REMOVE}_${(page-1)}`,
                toMark = `${C_THREAD_TO_REMOVE}_${(page)}`;
            try {
                // Remove threads previously marked for removal
                document.querySelectorAll(toRemove)
                    .forEach(e => e.parentNode.removeChild(e));

                // // Mark visible threads for removal on next iteration
                document.querySelectorAll(C_THREAD)
                    .forEach(e => e.className = toMark.replace(/\./g, ''));

            } catch (e) {
                _error("Unable to remove elements", e.message)
            }
        }

        // Scrolls to the bottom of the viewport
        function loadMore() {
            _log("Load more... page %d", page);
            window.scrollTo(0, document.body.scrollHeight);
        }

        // Recursive loop that ends when there are no more threads
        async function loop() {
            _log("Looping... %d entries added", threads.size);
            let size = await (scrapeThreads())
            if (size && page <= PAGE_TO_SCROLL) {
                try {
                    clearList();
                    loadMore();
                    page++;
                    setTimeout(loop, PAUSE)
                } catch (e) {
                    reject(e);
                }
            } else {
                _timeEnd("Scrape");
                resolve(Array.from(threads));
            }
        }
        loop();
    });
}