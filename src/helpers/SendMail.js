require('dotenv').config();

const nodemailer = require('nodemailer');

function mailNotifyError(errorMsg) {
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'ngohuuvandz@gmail.com',
            pass: 'huuvandeptrai'
        }
    });
    let text = `Error: ${errorMsg}`;
    // Step 2
    let mailOptions = {
        from: 'ngohuuvandz@gmail.com', // TODO: email sender
        to: 'huu.van.23tg@gmail.com', // TODO: email receivers
        subject: 'Error Notification',
        text
    };

    transporter.sendMail(mailOptions, (err) => {
        if (err) {
            console.log("Error: ", err);
        } else
            console.log("Success!");
    });
}

module.exports = function(links) {
    return new Promise((resolve, reject) => {
        // Step 1
        let transporter = nodemailer.createTransport({
            service: process.env.MAIL_SERVICE,
            auth: {
                user: process.env.MAIL_LOGIN,
                pass: process.env.MAIL_PASS
            }
        });
        let recipients = process.env.MAIL_RECIPIENTS.split(',');
        let text = `Đang có ${links.length} Incident đang cần được xử lý! \n ${links.join("\n\n")}`;
        // Step 2
        let mailOptions = {
            from: process.env.MAIL_LOGIN, // TODO: email sender
            to: recipients.join(','), // TODO: email receivers
            subject: 'Incident Notification',
            text
        };

        // Step 3
        transporter.sendMail(mailOptions, (err, data) => {
            if (err) {
                mailNotifyError(err);
                reject(err);
            }
            resolve();
        });
    });
};