module.exports = async function() {
        const CONTENTS = '.story_body_container p';
        const TIME = '._4g34 abbr'
        const POSTER = '.story_body_container header ._52we a';
        const REACTS = '.async_composer ._1g06'
        const SHARE = '.async_composer ._43lx._55wr'
        const HREF = 'href'

        const visibleThreads = await document.querySelector(CONTENTS)
        const postedTime = await document.querySelector(TIME)
        const poster = await document.querySelector(POSTER)
        const reacts = await document.querySelector(REACTS)
        const share = await document.querySelector(SHARE)
        return {
          contents: visibleThreads ? visibleThreads.innerText.trim() : '',
          postedTime: postedTime ? postedTime.innerText.trim() : '',
          poster: poster ? `https://facebook.com${((poster.getAttribute(HREF)).split('?'))[0]}` : '',
          reacts: reacts ? reacts.innerText.trim() : '',
          shares: share ? share.innerText.trim() : ''
        } 
}