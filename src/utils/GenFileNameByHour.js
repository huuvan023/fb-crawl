var date = new Date();
var seconds = date.getSeconds();
var minutes = date.getMinutes();
var hour = date.getHours();

module.exports = function () {
    return `${hour}-${minutes}-${seconds}`;
}