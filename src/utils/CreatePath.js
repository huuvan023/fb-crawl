const mkdirp = require('mkdirp');

module.exports = async function (path) {
    await mkdirp(`./${path}`)
}