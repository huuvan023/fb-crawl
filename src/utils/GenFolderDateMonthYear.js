var date = new Date();
var year = date.getFullYear();
var month = date.getMonth();
var day = date.getDate();

module.exports = function () {
    return `${day}-${month}-${year}`;
}