const fs = require('fs')

module.exports = async function (filename, output) {
    await fs.writeFile(filename, output, err => {
      if (err) {
        console.error(err)
        return
      }
    })
}