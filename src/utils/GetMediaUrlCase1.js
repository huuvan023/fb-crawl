module.exports = function() {
    return new Promise((resolve, reject) => {

        // Class for Individual Thread 
        // const C_THREAD = '.pagedlist_item:not(.pagedlist_hidden)';
        const C_THREAD = 'div._56be ';  //_55wo _5rgr _5gh8 async_like  
        // Class for threads marked for deletion on subsequent loop
        const C_THREAD_TO_REMOVE = 'div._56be .TO_REMOVE';
        const LINK_IMG = '._i81 > img';
        const LINK_VIDEO = 'video'

        const _log = console.info,
            _warn = console.warn,
            _error = console.error,
            _time = console.time,
            _timeEnd = console.timeEnd;

        _time("Scrape");

        let page = 1;

        // Global Set to store all entries
        let threads = new Set(); // Eliminates dupes

        // Pause between pagination
        const PAUSE = 5000;

        // Accepts a parent DOM element and extracts the title and URL
        function scrapeSingleThread(elThread) {
            try {
                const elLinkImg = elThread.querySelector(LINK_IMG) ?  elThread.querySelector(LINK_IMG) : null
                const elVid = elThread.querySelector(LINK_VIDEO) ? elThread.querySelector(LINK_VIDEO) : null
                // console.info('LINK: ', elVid)
                const link = elLinkImg ? elLinkImg.getAttribute('src') : elVid.getAttribute('src')
                threads.add(link);
            } catch (e) {
                _error("Error capturing individual thread", e);
            }
        }

        // Get all threads in the visible context
        function scrapeThreads() {
            _log(`Scraping page ${page}`);
            const visibleThreads = document.querySelectorAll(C_THREAD);

            if (visibleThreads.length > 0) {
                _log(`Scraping page ${page}... found ${visibleThreads.length} threads`);
                Array.from(visibleThreads).forEach(scrapeSingleThread);
            } else {
                _warn(`Scraping page ${page}... found no threads`);
            }

            // Return master list of threads;
            // resolve(Array.from(threads));
            return visibleThreads.length;
        }

        // Clears the list between pagination to preserve memory
        // Otherwise, browser starts to lag after about 1000 threads
        function clearList() {
            _log(`Clearing list page ${page}`);
            const toRemove = `${C_THREAD_TO_REMOVE}_${(page-1)}`,
                toMark = `${C_THREAD_TO_REMOVE}_${(page)}`;
            try {
                // Remove threads previously marked for removal
                document.querySelectorAll(toRemove)
                    .forEach(e => e.parentNode.removeChild(e));

                // // Mark visible threads for removal on next iteration
                document.querySelectorAll(C_THREAD)
                    .forEach(e => e.className = toMark.replace(/\./g, ''));

            } catch (e) {
                _error("Unable to remove elements", e.message)
            }
        }

        // Scrolls to the bottom of the viewport
        function loadMore() {
            _log("Load more... page %d", page);
            window.scrollTo(0, document.body.scrollHeight);
        }

        // Recursive loop that ends when there are no more threads
        async function loop() {
            _log("Looping... %d entries added", threads.size);
            let size = await(scrapeThreads())
            if (size > 0) {
                try {
                    clearList();
                    loadMore();
                    page++;
                    setTimeout(loop, PAUSE)
                } catch (e) {
                    reject(e);
                }
            } else {
                _timeEnd("Scrape");
                resolve(Array.from(threads));
            }
        }
        loop();
    });
}