module.exports = async function() {
    try {
        const IMGLINK = '.async_like .story_body_container ._39pi'
        var link = ''

        var image =  await document.querySelector(IMGLINK);
        link = image.getAttribute('href')
        return link
        
    }
    catch(e) {
        console.info('You get an error here ', e)
    }
    
}