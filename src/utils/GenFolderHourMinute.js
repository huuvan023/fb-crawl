var date = new Date();
var minutes = date.getMinutes();
var hour = date.getHours();

module.exports = function (path) {
    return `${hour}h${minutes}m`;
}