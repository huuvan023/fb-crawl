const { DownloaderHelper } = require('node-downloader-helper');
module.exports = async function(urlToDownload, videoName, savedPath) {    
    var options = {
        method: 'GET', // Request Method Verb
        headers: {},  // Custom HTTP Header ex: Authorization, User-Agent
        fileName: `${videoName}.mp4`,// Custom filename when saved
        // retry: false, // { maxRetries: number, delay: number in ms } or false to disable (default)
        // forceResume: false, // If the server does not return the "accept-ranges" header, can be force if it does support it
        // removeOnStop: true, // remove the file when is stopped (default:true)
        // removeOnFail: true, // remove the file when fail (default:true)
        // // override: true,
        // httpRequestOptions: {}, // Override the http request options  
        // httpsRequestOptions: {}, // Override the https request options, ex: to add SSL Certs
    };
    const dl = new DownloaderHelper(urlToDownload,savedPath, options);
    
    dl.on('end', () => console.log(`The video is finished downloading!`))
    dl.start();
}