module.exports = async function() {
    try {
        const IMGLINK = '.acbk i'
        const RETRY_VID = '.story_body_container ._53mw'
        var link = ''

        var checkImg =  await document.querySelectorAll(IMGLINK)
        if (checkImg.length > 0){
            await checkImg.forEach(async item => {
                if (item.getAttribute('role') === 'img') {
                    link = await item.getAttribute('data-store')
                    link =  JSON.parse(link).imgsrc
                }
                
            })
        } else {
            link = await JSON.parse(document.querySelector(RETRY_VID).getAttribute('data-store')).src
        }
        return link
        
    }
    catch(e) {
        console.info('You get an error here ', e)
    }
    
}