const fs = require('fs');

module.exports = async function (path) {
    fs.rmdir(path, (err) => {
        if (err) {
            return console.log("Error occurred in deleting directory", err);
        }
    });
}
