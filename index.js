// chromium-browser --remote-debugging-port=21222

var customId = require("custom-id");
const puppeteer = require('puppeteer')
const { writeFileSync, readFileSync, existsSync } = require("fs");
require('dotenv').config();
const fs = require('fs')
const converter = require('json-2-csv');

const script = require('./src/main/script');
const createPath = require("./src/utils/CreatePath");
const witeFile = require("./src/utils/WriteFile");
const getFolder = require('./src/utils/GenFolderDateMonthYear');
const getLogFileName = require('./src/utils/GenFileNameByHour');
const getFolderByHourMinute = require('./src/utils/GenFolderHourMinute');
const saveVideo = require("./src/utils/SaveVideo");
const saveImage = require('./src/utils/SaveImage');
const sleep = require('./src/utils/Sleep');
const getUnknownTypeContent = require('./src/utils/GetUnknownTypeContent');
const getContent = require('./src/utils/GetContent');
const getMediaUrlCase1 = require('./src/utils/GetMediaUrlCase1');
const getMediaUrlCase2 = require('./src/utils/GetMediaUrlCase2');
const removeFolder = require('./src/utils/RemoveFolder');
const convertToNonAccent = require('./src/helpers/Convert');
const sendEmail = require('./src/helpers/SendMail');
const SendMail = require("./src/helpers/SendMail");
const { getBrowserInstance } = require('./src/main/instance');
const sendMailToNotifyCheckpoint = require('./src/helpers/NotifyCheckpoint');

let folderToSave = '';
let page = null;
let browser = null;

Date.prototype.yyyymmdd = function() {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return `${[this.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
           ].join('-')} ${this.getHours()}:${this.getMinutes()}`
};

(async() => {
    // const browserURL = 'http://127.0.0.1:21222';
    // const browser = await puppeteer.connect({ browserURL });
    try {
        console.log("Execute time:", (new Date()).yyyymmdd());
        console.log("Preparing to open browser...")
        browser = await getBrowserInstance();
        console.log("Preparing to open new page...");
        page = await browser.newPage();
        console.log("Setting width and height viewport...")
        await page.setViewport({ width: 1366, height: 768 });
        console.log("Preparing to goto url...")
        await page.goto(process.env.LOGIN_URL);
        console.log("Preparing to login...")
        await login();
        let checkpointFb = await checkFBCheckpoint();
        if (checkpointFb) {
            sendMailToNotifyCheckpoint();
        } else {
            console.log("No checkpoint detected!");
        }
        console.log("Sleep 5s...");
        await sleep(5000)
            //============Starting loop===============
        let listTargetUrl = process.env.TARGET_URLS.split(',');

        for (let index = 0; index < listTargetUrl.length; index++) {
            let URLTarget = listTargetUrl[index];
            await page.goto(URLTarget);
            await sleep(5000);
            page.waitForNavigation({ waitUntil: 'networkidle2' });
            console.log("GROUP - ", index);
            console.log('\n\nCRAWLING THREADS LINK\n\n');
            const threads = await page.evaluate(script);

            if (threads.length == 0) {
                console.log("\n===================================================================\n")
                console.log("=========================== NO THREAD AVAIABLE! ==========================")
                console.log("\n===================================================================\n");
                continue;
            }
            console.info("Total threads collected:", threads.length)
            console.log("\n===================================================================\n")
            console.log("=========================== STARTING TO SCRAWL ==========================")
            console.log("\n===================================================================\n");

            let groupId = URLTarget.split('groups/')[1];
            for (let i = 0; i < threads.length; i++) {
                let scrapePage = await browser.newPage();
                await scrapePage.setViewport({ width: 1366, height: 768 });
                console.log('\n\n\n-------------------------Starting....-----------------------------');
                console.log('Thread number: ', i)
                await onScrape(threads[i], scrapePage, groupId);
                console.log('--------------------------End----------------------------\n\n\n');
            }
        }
        let checkFileIsExist = await existsSync(`${folderToSave}/posts.json`);
        if (checkFileIsExist) {
            let fileData = await readFileSync(`${folderToSave}/posts.json`);
            let links = [];
            try {
                links = JSON.parse(fileData).map(item => item.source);
            } catch (e) {
                console.log("Something went wrong when parse file json!");
                await page.close();
                await browser.close();
            }
            //Sending mail..
            if (links.length > 0) {
                SendMail(links).then(rs => {
                        console.log("SENDING EMAIL SUCCESSFULLY!");
                    })
                    .catch(err => {
                        console.log(err);
                    });
            } else {
                console.log("ERROR: Error when mapping links")
            }
        }
        await page.close();
        await browser.close();
    } catch (e) {
        console.log("\n\n\n-----------------------------------------------ERROR!!!-----------------------------------------------\n\n\n")
        console.log(e);
        if (browser) {
            browser.close();
        }
    }
})();

async function onScrape(crawlData, scrapePage, groupId) {
    var ID = null
    var bodyContent = '';
    var responseType = 'complete';
    var imageCount = 0;
    var videoCount = 0;
    var type = null;
    var responseData = {}

    //Generate folder
    let wrapFolder = getFolder();
    let folder = getFolderByHourMinute();
    folderToSave = `out/${wrapFolder}/${folder}`
    await createPath(folderToSave);
    //Create folder to save posts
    await createPath(`${folderToSave}/GROUP_${groupId}`);

    try {
        //Gen ID
        ID = customId({ randomLength: 4 })
            // await createPath(`${folderToSave}/${ID}`);
        if ((crawlData.url).includes('attachment_style.')) {

            type = ((crawlData.url).split("attachment_style.")[1]).split("%3")[0] || 'Unknown!';
            console.info(`This is ${type} post`);
            await scrapePage.goto(crawlData.url);

            if (type == 'share') {
                await scrapePage.waitForSelector('body', {
                    visible: true,
                });
                await scrapePage.evaluate(getUnknownTypeContent).then(rs => {
                    bodyContent = rs
                });
                responseType = 'complete';
                if (!checkIsContainsKeyWords(bodyContent.contents)) {
                    console.warn("No keywords in this post!");
                    // removeFolder(`${folderToSave}/${ID}`);
                    await sleep(5000);
                    scrapePage.close();
                    return;
                }
            } else if (type !== 'video_inline') {
                await scrapePage.waitForSelector('body', { visible: true });

                await scrapePage.evaluate(getContent).then(rs => { bodyContent = rs })
                    .then(() => { return scrapePage.waitForSelector('.story_body_container', { visible: true }) })
                    .then(() => { return scrapePage.click('.story_body_container ._403j') })
                    .catch(e => { console.info("Error: ", e) })

                if (!checkIsContainsKeyWords(bodyContent.contents)) {
                    console.warn("No keywords in this post!");
                    // removeFolder(`${folderToSave}/${ID}`);
                    await sleep(5000);
                    scrapePage.close();
                    return;
                }

                const url = await scrapePage.url();
                if (url.length > 150) {
                    await scrapePage.waitForSelector('._56be', { visible: true })
                    const mediaLinks = await scrapePage.evaluate(getMediaUrlCase1)
                    for (let k = 0; k < mediaLinks.length; k++) {
                        if (mediaLinks[k].includes('mp4')) {
                            videoCount++;
                            // await saveVideo(mediaLinks[k], `video_${ID}_${videoCount}`, `${folderToSave}/${ID}`)
                        } else {
                            imageCount++
                            // await saveImage(mediaLinks[k], `${folderToSave}/${ID}/image_${ID}_${imageCount}.jpg`);
                        }
                    }
                } else {
                    //Case post with list image was put as carousel
                    await scrapePage.waitForSelector('body', { visible: true });

                    var listMediaLinksProcessed = []
                    var currentMediaTarget = ''

                    // Loop to crawl all image
                    var a = 0
                    var check = true;
                    while (check) {
                        a++;
                        await scrapePage.waitForSelector('.async_like', {
                            visible: true,
                        });
                        await sleep(3000);
                        //Get image source
                        await scrapePage.evaluate(getMediaUrlCase2).then(rs => { currentMediaTarget = rs })
                        if (listMediaLinksProcessed.includes(currentMediaTarget)) {
                            check = false
                            break;
                        }

                        listMediaLinksProcessed.push(currentMediaTarget)

                        //If media is a video
                        if (currentMediaTarget.includes('mp4')) {
                            videoCount++;
                            // await saveVideo(currentMediaTarget, `video_${ID}_${videoCount}`, `${folderToSave}/${ID}`)
                            responseType = 'need to check'
                            break;
                        } else {
                            imageCount++
                            // await saveImage(currentMediaTarget, `${folderToSave}/${ID}/image_${ID}_${imageCount}.jpg`);
                        }

                        await scrapePage.waitForSelector('.async_like', { visible: true });
                        try {
                            await scrapePage.click('.acbk > div._57-p > a');
                        } catch (e) {}
                        await sleep(1000)
                    }
                }
            } else {
                await scrapePage.evaluate(getContent).then(rs => {
                    bodyContent = rs
                })
                if (!checkIsContainsKeyWords(bodyContent.contents)) {
                    console.warn("No keywords in this post!");
                    // removeFolder(`${folderToSave}/${ID}`);
                    await sleep(5000);
                    scrapePage.close();
                    return;
                }
                videoCount++;
                // var vid_Url = await scrapePage.evaluate(() => {
                //         let video = document.querySelector('.story_body_container section > div > div')
                //         return JSON.parse(video.getAttribute('data-store')).src
                //     })
                // await saveVideo(vid_Url, `video_${ID}_${videoCount}`, `${folderToSave}/${ID}`);
            }

        } else {
            console.info('Maybe this is share post!!');
            await scrapePage.goto(crawlData.url);
            await scrapePage.waitForSelector('body', {
                visible: true,
            });
            await scrapePage.evaluate(getUnknownTypeContent).then(rs => {
                bodyContent = rs
            });
            if (!checkIsContainsKeyWords(bodyContent.contents)) {
                console.warn("No keywords in this post!");
                // removeFolder(`${folderToSave}/${ID}`);
                await sleep(5000);
                scrapePage.close();
                return;
            }
            responseType = 'need to check';
            type = 'Unknown!';
        }
        responseData = {
            ID: ID,
            responseType,
            source: crawlData.url,
            type: type,
            ...bodyContent,
            images: imageCount,
            videos: videoCount
        }
    } catch (e) {
        console.info('-------------------------------------------------------------')
        console.info('ERROR: ', e)
        responseData = {
            ID: ID,
            responseType: 'need to check',
            source: crawlData.url,
            type: type,
            ...bodyContent,
            images: imageCount,
            videos: videoCount,
        }
    }
    await writeOutPutToFile(`${folderToSave}/GROUP_${groupId}/`, folderToSave, responseData);
    console.info('Sleeping 5s...');
    await sleep(5000);
    scrapePage.close();
}
async function checkFBCheckpoint() {
    await page.waitForSelector('body', {
        visible: true,
    });
    if (page.url() && page.url().includes("checkpoint")) {
        return true;
    }
    return false;
}
async function login() {
    if (!page) {
        console.error("\n\n\n----------------------------------No page found!----------------------------------\n\n\n");
        return;
    }
    console.log("Logging in....")
    page.waitForSelector('input[name=email]');
    page.waitForSelector('input[name=pass]');
    let loginname = process.env.FB_LOGINNAME;
    let pass = process.env.FB_PASS;
    await page.$eval('input[name=email]', (el, loginname) => el.value = loginname, loginname);
    await page.$eval('input[name=pass]', (el, pass) => el.value = pass, pass);

    page.waitForSelector('button[type="button"]');

    await page.click("button[type='button']");

    try {
        if ((await page.waitForXPath('//*[contains(text(), "Log in with one tap")]', 3000)) !== null ||
            (await page.waitForXPath('//*[contains(text(), "Đăng nhập bằng một lần nhấn")]', 3000)) !== null) {
            page.click('button[type="submit"]');
        }
    } catch (e) {
        page.click('button[type="submit"]');
    }
    console.log("Logged in successfully!")
}
async function writeOutPutToFile(filePath, folderToSave, addedData) {
    let checkFileExist = await existsSync(`${filePath}posts.json`);
    let writtenData = null;
    if (checkFileExist) {
        let file = await readFileSync(`${filePath}posts.json`);
        let fileData = JSON.parse(file);
        if (Array.isArray(fileData)) {
            fileData.push(addedData);
        } else if (typeof fileData == 'object') {
            fileData = {...fileData, ...addedData };
        } else {
            fileData = fileData.concat(addedData);
        }
        writtenData = fileData;
        const csv = await converter.json2csvAsync(writtenData);
        await writeFileSync(`${filePath}posts.json`, JSON.stringify(writtenData));
        await writeFileSync(`${filePath}posts.csv`, csv);
    } else {
        const csv = await converter.json2csvAsync([addedData]);
        writtenData = [addedData];
        await writeFileSync(`${filePath}posts.json`, JSON.stringify(writtenData));
        await writeFileSync(`${filePath}posts.csv`, csv);
    };

    //Write date to 1 file
    let checkFile = await existsSync(`${folderToSave}/posts.json`)
    if (checkFile) {
        let file = await readFileSync(`${folderToSave}/posts.json`);
        let fileData = JSON.parse(file);
        if (Array.isArray(fileData)) {
            fileData.push(addedData);
        } else if (typeof fileData == 'object') {
            fileData = {...fileData, ...addedData };
        } else {
            fileData = fileData.concat(addedData);
        }
        await writeFileSync(`${folderToSave}/posts.json`, JSON.stringify(fileData));
    } else {
        await writeFileSync(`${folderToSave}/posts.json`, JSON.stringify([addedData]));
    }
    console.info('SAVE POST SUCCESSFULLY!');
};

function checkIsContainsKeyWords(contents) {
    let listKeywords = process.env.KEYWORDS.split(',');
    let paragraph = convertToNonAccent(contents);

    for (let index = 0; index < listKeywords.length; index++) {
        const str = convertToNonAccent(listKeywords[index]);

        if (paragraph.includes(str)) {
            return true;
        }
    }
    return false;
}