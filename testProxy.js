const puppeteer = require('puppeteer');
require('dotenv').config();

(async() => {
    const browser = await puppeteer.launch({
        headless: true,
        executablePath: process.env.EXECUTABLE_PATH,
        args: ['--proxy-server=http://10.10.10.10:8000']
    });
    const page = await browser.newPage();
    await page.goto('http://toscrape.com');
    await page.screenshot({ path: 'example.png' });
    await browser.close();
})();